import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            //edit here
            try{
                System.out.print("masukkan pembilang : ");
                int pembilang = scanner.nextInt();

                System.out.print("masukkan penyebut : ");
                int penyebut = scanner.nextInt();

                int hasil = pembagian(pembilang, penyebut);
                System.out.println("hasil pembagian = "+hasil);
                validInput = true;
            }
            
            catch(InputMismatchException ex){
                System.out.println("input tidak valid, masukkan bilangan bulat");
                scanner.nextLine();
            }
            catch (ArithmeticException ex){
                System.out.println(ex.getMessage());
            }
            
        } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if(penyebut == 0){
            throw new ArithmeticException("pembagian dengan penyebut 0 tidak terdefinisi, masukkan bilangan bulat");
        }
        return pembilang / penyebut;
    }
}
